package me.moehritz.labypresence;

import com.github.psnrigner.*;
import net.labymod.api.LabyModAddon;
import net.labymod.main.LabyMod;
import net.labymod.main.Source;
import net.labymod.servermanager.Server;
import net.labymod.utils.Debug;

public class LabyPresence extends LabyModAddon {
    private static final String[] servers = new String[]{"timolia", "gommehd", "hypixel"};

    private DiscordRpc discordRpc;

    private boolean thread = false;
    private boolean enabled = false;

    private String lastServer;
    private long joinedAt;

    @Override
    public void onEnable() {
        discordRpc = new DiscordRpc();

        enabled = true;

        if (!thread) {
            thread = true;
            Thread discord = new Thread(() -> {
                try {
                    Debug.log(Debug.EnumDebugMode.ADDON, "Loading DISCORD");
                    discordRpc.init("381728241263575040", new DiscordEventHandler() {
                        @Override
                        public void ready() {
                            Debug.log(Debug.EnumDebugMode.ADDON, "Discord READY");
                        }

                        @Override
                        public void disconnected(ErrorCode errorCode, String s) {
                            Debug.log(Debug.EnumDebugMode.ADDON, String.format("Discord DISCONNECTED (%s): %s", errorCode.name(), s));
                        }

                        @Override
                        public void errored(ErrorCode errorCode, String s) {
                            Debug.log(Debug.EnumDebugMode.ADDON, String.format("Discord ERROR (%s): %s", errorCode.name(), s));
                        }

                        @Override
                        public void joinGame(String s) {
                            LabyMod.getInstance().connectToServer(s);
                        }

                        @Override
                        public void spectateGame(String s) {
                            // NEEDS DISCORD APPROVAL
                            Debug.log(Debug.EnumDebugMode.ADDON, String.format("Discord SPECTATE: %s", s));
                        }

                        @Override
                        public void joinRequest(DiscordJoinRequest discordJoinRequest) {
                            // NEEDS DISCORD APPROVAL
                            Debug.log(Debug.EnumDebugMode.ADDON, String.format("Discord REQUEST: %s", discordJoinRequest.getUsername()));
                        }
                    }, true, null);
                    Thread.sleep(5000L);
                    Debug.log(Debug.EnumDebugMode.ADDON, "Executing discord callbacks");
                    discordRpc.runCallbacks();

                    while (true) {
                        if (enabled) {
                            String ip = LabyMod.getInstance().getLastServerIp();
                            DataBuilder builder = new DataBuilder()
                                    .details("LabyMod " + Source.ABOUT_VERSION)
                                    .largeImageKey("labymod")
                                    .largeImageText("labymod.net")
                                    .joinSecret(ip)
                                    .state("Ingame");
                            if (ip == null || !LabyMod.getInstance().isInGame()) {
                                joinedAt = -1;
                            } else {
                                if (joinedAt == -1 || !ip.equals(lastServer)) {
                                    joinedAt = System.currentTimeMillis() / 1000;
                                }
                                lastServer = ip;
                                builder.startTimestamp(joinedAt);
                                for (String s : servers) {
                                    if (ip.contains(s)) {
                                        builder.largeImageKey(s)
                                                .smallImageKey("labymod_small")
                                                .smallImageText("labymod.net");
                                        Server currentServer = LabyMod.getInstance().getServerManager().getCurrentServer();
                                        if (currentServer != null) {
                                            builder.largeImageText(currentServer.getName());
                                        } else {
                                            builder.largeImageText("Online auf " + ip);
                                        }
                                        break;
                                    }
                                }
                            }
                            Debug.log(Debug.EnumDebugMode.ADDON, "Updateing DISCORD");
                            updateStatus(builder);
                        }
                        Thread.sleep(5000L);
                    }
                } catch (InterruptedException ignored) {
                }
            }, "discord_rpc");
            discord.start();
            Runtime.getRuntime().addShutdownHook(new Thread(this::onDisable));
        }
    }

    public void updateStatus(DataBuilder builder) {
        updateStatus(builder.drp());
    }

    public void updateStatus(DiscordRichPresence presence) {
        discordRpc.updatePresence(presence);
        discordRpc.runCallbacks();
    }

    @Override
    public void onDisable() {
        enabled = false;
        discordRpc.shutdown();
    }
}
