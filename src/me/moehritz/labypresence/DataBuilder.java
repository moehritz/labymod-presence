package me.moehritz.labypresence;

import com.github.psnrigner.DiscordRichPresence;

public class DataBuilder {
    private String state = null;
    private String details = null;
    private long startTimestamp = 0L;
    private long endTimestamp = 0L;
    private String largeImageKey = null;
    private String largeImageText = null;
    private String smallImageKey = null;
    private String smallImageText = null;
    private String partyId = null;
    private int partySize = 0;
    private int partyMax = 0;
    private String matchSecret = null;
    private String joinSecret = null;
    private String spectateSecret = null;
    private boolean instance = false;

    public DataBuilder() {
    }

    public DataBuilder state(String state) {
        this.state = state;
        return this;
    }

    public DataBuilder details(String details) {
        this.details = details;
        return this;
    }

    public DataBuilder startTimestamp(long startTimestamp) {
        this.startTimestamp = startTimestamp;
        return this;
    }

    public DataBuilder endTimestamp(long endTimestamp) {
        this.endTimestamp = endTimestamp;
        return this;
    }

    public DataBuilder largeImageKey(String largeImageKey) {
        this.largeImageKey = largeImageKey;
        return this;
    }

    public DataBuilder largeImageText(String largeImageText) {
        this.largeImageText = largeImageText;
        return this;
    }

    public DataBuilder smallImageKey(String smallImageKey) {
        this.smallImageKey = smallImageKey;
        return this;
    }

    public DataBuilder smallImageText(String smallImageText) {
        this.smallImageText = smallImageText;
        return this;
    }

    public DataBuilder partyId(String partyId) {
        this.partyId = partyId;
        return this;
    }

    public DataBuilder partySize(int partySize) {
        this.partySize = partySize;
        return this;
    }

    public DataBuilder partyMax(int partyMax) {
        this.partyMax = partyMax;
        return this;
    }

    public DataBuilder matchSecret(String matchSecret) {
        this.matchSecret = matchSecret;
        return this;
    }

    public DataBuilder joinSecret(String joinSecret) {
        this.joinSecret = joinSecret;
        return this;
    }

    public DataBuilder spectateSecret(String spectateSecret) {
        this.spectateSecret = spectateSecret;
        return this;
    }

    public DataBuilder instance(boolean instance) {
        this.instance = instance;
        return this;
    }

    public DiscordRichPresence drp() {
        DiscordRichPresence drp = new DiscordRichPresence();
        drp.setState(state);
        drp.setDetails(details);
        drp.setStartTimestamp(startTimestamp);
        drp.setEndTimestamp(endTimestamp);
        drp.setLargeImageKey(largeImageKey);
        drp.setLargeImageText(largeImageText);
        drp.setSmallImageKey(smallImageKey);
        drp.setSmallImageText(smallImageText);
        drp.setPartyId(partyId);
        drp.setPartySize(partySize);
        drp.setPartyMax(partyMax);
        drp.setMatchSecret(matchSecret);
        drp.setJoinSecret(joinSecret);
        drp.setSpectateSecret(spectateSecret);
        drp.setInstance(instance);
        return drp;
    }
}
